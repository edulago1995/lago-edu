console.log("Hello World!");

/*
    1. Create a function named getUserInfo which is able to return an object. 

        The object returned should have the following properties:
        
        - key - data type

        - name - String
        - age -  Number
        - address - String
        - isMarried - Boolean
        - petName - String

        Note: Property names given is required and should not be changed.

        To check, create a variable to save the value returned by the function.
        Then log the variable in the console.

        Note: This is optional.

*/

        function getUserInfo() {
          let userInfo = {
            name: "Edu",
            age: 28,
            address: "408 2nd Street Samploc",
            isMarried: false,
            petName: "Summer",
          };
          return userInfo;
        }

        let getUserInfos = getUserInfo();
        console.log("getUserInfo();");
        console.log(getUserInfos);

/*
    2. Create a function named getArtistsArray which is able to return an array with at least 5 names of your favorite bands or artists.
        
        - Note: the array returned should have at least 5 elements as strings.
                function name given is required and cannot be changed.


        To check, create a variable to save the value returned by the function.
        Then log the variable in the console.

        Note: This is optional.
    
*/

        function getArtistsArray() {
          return ["The Beatles", "Led Zeppelin", "Pink Floyd", "The Rolling Stones", "Metallica"];
        }

        let getArtistsArrays = getArtistsArray();
        console.log("getArtistsArray();");
        console.log(getArtistsArrays);

/*
    3. Create a function named getSongsArray which is able to return an array with at least 5 titles of your favorite songs.

        - Note: the array returned should have at least 5 elements as strings.
                function name given is required and cannot be changed.

        To check, create a variable to save the value returned by the function.
        Then log the variable in the console.

        Note: This is optional.
*/

        function getSongsArray() {
          return [
            "Flowers",
            "As It Was",
            "Kill Bill",
            "Anti-Hero",
            "Daylight",
          ];
        }

        let getSongsArrays = getSongsArray();
        console.log("getSongsArray();");
        console.log(getSongsArrays);
/*
    4. Create a function named getMoviesArray which is able to return an array with at least 5 titles of your favorite movies.

        - Note: the array returned should have at least 5 elements as strings.
                function name given is required and cannot be changed.

        To check, create a variable to save the value returned by the function.
        Then log the variable in the console.

        Note: This is optional.
*/

        function getMoviesArray() {
          return [
            "The Godfathery",
            "The Dark Knight",
            "The Shawshank Redemption",
            "The Godfather Part II",
            "Schindler's List",
          ];
        }

        let getMoviesArrays = getMoviesArray();
        console.log("getMoviesArray();");
        console.log(getMoviesArrays);

/*
    5. Create a function named getPrimeNumberArray which is able to return an array with at least 5 prime numbers.

            - Note: the array returned should have numbers only.
                    function name given is required and cannot be changed.

            To check, create a variable to save the value returned by the function.
            Then log the variable in the console.

            Note: This is optional.
            
*/

        function getPrimeNumberArray() {
          return [2, 3, 5, 7, 11];
        }

        let getPrimeNumberArrays = getPrimeNumberArray();
        console.log("getPrimeNumberArray();");
        console.log(getPrimeNumberArrays);


//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try {
  module.exports = {
    getUserInfo: typeof getUserInfo !== "undefined" ? getUserInfo : null,
    getArtistsArray:
      typeof getArtistsArray !== "undefined" ? getArtistsArray : null,
    getSongsArray: typeof getSongsArray !== "undefined" ? getSongsArray : null,
    getMoviesArray:
      typeof getMoviesArray !== "undefined" ? getMoviesArray : null,
    getPrimeNumberArray:
      typeof getPrimeNumberArray !== "undefined" ? getPrimeNumberArray : null,
  };
} catch (err) {}
