console.log("Hi, B297!");

//Mini-Activity - log your favorite movie line 20 times in the console.
//Send a screenshot of your console.

function printLine(){
	console.log("'Carpe diem. Seize the day, boys. Make your lives extraordinary.' -Dead Poets Society, 1989");
};

printLine();

//Functions
//lines/blocks of code that tell our devices to perform a certain task when called/invoked

//Function declaration

/*
	Syntax:

	function functionName() {
		code block (statement)
	}

*/
	//Function Declaration
	function printName(){
		console.log("My name is Cee.");
	};

	//Function Invocation
	printName();

	declaredFunction(); 

	//Function declaration vs expressions

	//Function Declaration
		//function declaration is created with the function keyword and adding a function name
	//they are "saved for later use"

	function declaredFunction(){
		console.log("Hello from declaredFunction!")
	};

	declaredFunction();

	//Function Expression
		//function expression is stored in a variable
		//function expression is an anonymous function assigned to the variable

	//variableFunction();//Uncaught ReferenceError: Cannot access 'variableFunction' before initialization

	let variableFunction = function() {
		console.log("Hello from function expression!")
	};

	variableFunction();

	//a function expression of function named funcName assigned to the variable funcExpression

	let funcExpression = function funcName() {
		console.log("Hello from the other side!");
	};

	funcExpression();

	//We can also reassign declared functions and function expressions to new anonymous functions

	declaredFunction = function(){
		console.log("updated declaredFunction");
	};

	declaredFunction();

	funcExpression = function(){
		console.log("updated funcExpression");
	}

	funcExpression();


	const constantFunc = function(){
		console.log("Initialized with const!")
	};

	constantFunc();

	/*constantFunc = function(){
		console.log("Cannot be reassigned!")
	}

	constantFunc();*/

	//Function Scoping

	/*
		Scope - acessibility/visibility of variables

		JS Variables has 3 types of scope:
		1. local/block scope
		2. global scope
		3. function scope


	*/
/*
	{
		let a = 1;
	}

	let a = 1;

	function sample(){
		let a = 1;
	}

*/

	{
		let localVar = "Armando Perez";
	}

	let globalVar = "Mr. Worldwide";

	//console.log(localVar);//result in an error
	console.log(globalVar);

	function showNames() {

		var functionVar = "Joe";
		const functionConst = "John";
		let functionLet = "Jane";

		console.log(functionVar);
		console.log(functionConst);
		console.log(functionLet);
	};

/*	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);*/

	showNames();


	//Nested Functions

	function myNewFunction(){

		let name = "Jane";

		function nestedFunction(){
			let nestedName = "John";
			console.log(name);
		}

		//console.log(nestedName);

		nestedFunction();
	}

	myNewFunction();
	//nestedFunction();//is declared inside the myNewFunction scope

	//Function and Global Scoped Variables

	//Global Scoped Variable
	
	let globalName = "Cardo";

	function myNewFunction2(){
		let nameInside = "Hillary"
		console.log(globalName);
	};

	myNewFunction2();
	//console.log(nameInside);


	// Using alert()

	//alert("This will run immediately when the page loads.")
	function showSampleAlert(){
		alert("Hello, Earthlings! This is from a function!")
	}
	//showSampleAlert();

	console.log("I will only log in the console when the alert is dismissed!");

	// Using prompt()

	let samplePrompt = prompt("Enter your Name: ");

	//console.log("Hi, I am " + samplePrompt);

	//prompt returns an empty string when there is no input. or null if the user cancels the prompt()

	function printWelcomeMessage(){
		let firstName = prompt("Enter your first name: ");
		let lastName = prompt("Enter your last name: ");

		console.log("Hello, " + firstName + " " + lastName + "!");
		console.log("Welcome to my page!")
	}

	printWelcomeMessage();

	//The Return Statement
	/*
		The return statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function
	*/

	function returnFullName(){
		return "Jeffrey" + ' ' + "Smith" + ' ' + "Bezos";
		console.log("This message will not be printed!");
	}

	let fullName = returnFullName();
	console.log(fullName);


	function returnFullAddress(){
		let fullAddress = {

			street : "#44 Maharlika St.",
			city: "Cainta",
			province: "Rizal"
		};

		return fullAddress;
	}

	let myAddress = returnFullAddress();
	console.log(myAddress);

	function printPlayerInfo(){

		console.log("Username: " + "dark_magician");
		console.log("Level: " + 95);
		console.log("Job: " + "Mage");

	}

	let user1 = printPlayerInfo();
	console.log(user1);//undefined

	function returnSumOf5and10(){
		return 5 + 10;
	}

	let sumOf5And10 = returnSumOf5and10();//15
	console.log(sumOf5And10);

	let total = 100 + returnSumOf5and10();//115
	console.log(total);


	function getGuildMembers(){

		return ["Lulu","Tristana","Teemo"];
	}

	console.log(getGuildMembers());


	//Function Naming Conventions

	function getCourses(){
		let courses = ["ReactJs 101","ExpressJs 101","MongoDB 101"];
		return courses;
	}

	let courses = getCourses();
	console.log(courses);

	//Avoid using generic names and pointless and inappropriate function names
	function get(){
		let color = "pink";
		return name;
	}

	//use camelCase
	function displayCarInfo(){
		console.log("Brand: Toyota");
		console.log("Type: Sedan");
	}

	displayCarInfo();