console.log("Hello, B297!");

//Functions
	//Parameters and Arguments

/*	function printName(){

		let nickname = prompt("Enter your nickname: ");
		console.log("Hi, " + nickname);

	}

	printName();*/

	function printName(name){

		console.log("Hi, " + name);

	}

	printName("Cee");


	let sampleVariable = "Cardo";

	printName(sampleVariable);



	function checkDivisibilityBy8(num){

		let remainder = num % 8;
		console.log("The remainder of " + num + " divided by 8 is: " + remainder);
		let isDivisibleBy8 = remainder === 0;
		console.log("Is " + num + " divisible by 8?");
		console.log(isDivisibleBy8);
	}

	checkDivisibilityBy8(64);
	checkDivisibilityBy8(28);
	checkDivisibilityBy8(9678);

	/*
		Mini-Activity (5 min.)
		check the divisibility of a number by 4
		have one parameter named num

		1. 56
		2. 95
		3. 444

	*/

	function checkDivisibilityBy4(num){

		let remainder = num % 4;
		console.log("The remainder of " + num + " divided by 4 is: " + remainder);
		let isDivisibleBy4 = remainder === 0;
		console.log("Is " + num + " divisible by 4?");
		console.log(isDivisibleBy4);
	}

	checkDivisibilityBy4(56);
	checkDivisibilityBy4(95);
	checkDivisibilityBy4(444);


	//Functions as arguments
	//Function parameters can also accept other functions as arguments

	function argumentFunction(){
		console.log("This function was passed as an argument before the message was printed.");
	};

	function invokeFunction(argumentFunction){
		argumentFunction();
	}

	//Adding and removing the parentheses "()" impacts the output of JS heavily
	//when a function is used with parentheses "()", it denotes invoking/calling a function
	//A function used without a parenthesis is normally associated with using the function as an argument to another function

	invokeFunction(argumentFunction);

	//Using multiple parameters

	function createFullName(firstName, middleName, lastName){
		console.log(firstName + ' ' + middleName + ' ' + lastName);
	};

	createFullName('Juan','Dela','Cruz');
	createFullName('Cruz','Dela','Juan');
	createFullName('Juan','Dela');
	createFullName('Juan','','Cruz');
	createFullName('Juan','Dela','Cruz','III');

	//Using variables as arguments

	let firstName = "John";
	let middleName = "Doe";
	let lastName = "Smith";

	createFullName(firstName,middleName,lastName);

	/*
		create a function called printFriends
		
		3 parameters
		friend1, friend2, friend3

		3 mins.

	*/
	function printFriends(friend1,friend2,friend3){
			console.log("My three friends are: " + friend1 + ", "+ friend2 + ", " + friend3 + ".")
		}

		printFriends("Amy", "Lulu", "Morgan");


	//Return Statement


	function returnFullName(firstName,middleName,lastName){

		return firstName + ' ' + middleName + ' ' + lastName;
		console.log("This will not be printed!");


	}

	let completeName1 = returnFullName("Monkey","D","Luffy");
	let completeName2 = returnFullName("Cardo","Tanggol","Dalisay");

	console.log(completeName1 + " is my bestfriend!");
	console.log(completeName2 + " is my friend!");


	/*
		Mini Activity

		1. Create a function that will calculate an area of a square (5mins)
		2. Create a function that will add 3 numbers (3 mins)
		3. Create a function that will check if the number is equal to 100 (3 mins)

	*/

	function getSquareArea(side){

		return side**2
	};

	let areaSquare = getSquareArea(4);

	console.log("The result of getting the area of a square with length of 4:")
	console.log(areaSquare);//16

	function computeSum(num1,num2,num3){

		return num1 + num2 + num3;
	}

	let sumOfThreeDigits = computeSum(1,2,3);
	console.log("The sum of theree numbers are: ");
	console.log(sumOfThreeDigits);//6

	function compareToOneHundred(num){

		return num === 100;

	}

	let booleanValue = compareToOneHundred(99);
	console.log("Is this one hundred?");
	console.log(booleanValue);



// OTHER WAY

	function calculateSqaure(side){

		let area = side * side;
		console.log("The area of a sqaure with the side lengts of " + side + " is eqaul to: " + area);
		return side;
	}

	calculateSqaure(12);

	function calculateNum(num1,num2,num3){

		let sum = num1 + num2 + num3;
		console.log("The total of the three number is " + sum);
		return sum;
	}

	calculateNum(3,5,6);


	function isEqual(num){

		let equal = num === 100;
		console.log("is " + num + " is eqaul to 100?\n" + equal);
		return equal;
	}

	isEqual(30);