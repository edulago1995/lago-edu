console.log("Hello, B297")

//[if, else if, and else statements]

let numG = -1;


// if statement
// executes a statements if a specified condition is true

if(numG < 0){
	console.log("Hellow! The condition in the if statements	is true!!")
}

let numH = 1;

// else if clause
// execute a statement if the previous condition if false and if the specified condition is true
// the else if clause is OPTIONAL and can be added to capture additional condition to change the flow of the program!

if(numG > 0){
	console.log("Hello!")
}
else if(numH > 0){
	console.log("This will log if the else if condition is true and and if condition is true!")
}


// else statement
// executes a statement if all other conditions are false
// the "else" statements is OPTIONAL and can be added to capture any other result to change the flow of our program

if (numG > 0){
	console.log("I'm enchanted to meet you!")
}
else if (numH = 0){
	console.log("It's me, Hi...")
}
else {
	console.log("Hello from the other side! This will log if the if and else if condition are not met!")
}

// [if, else if, and else statements with funtions]

let message = 'No message!';
console.log(message);

	function determineTyphoonIntensity(windSpeed){

		if (windSpeed < 30){
			return 'Not a typhoon yet!'
		}

		else if (windSpeed <= 61){
			return 'Tropical Depression detected!'
		}
		else if (windSpeed >=62 && windSpeed <=88){
			return 'Tropical Strom detected!'
		}
		else if (windSpeed >=89 && windSpeed <=117){
			return 'Severe Tropical Strom detected!'
		}
		else {
			return 'Typhoon detected!'
		}
	}

	message = determineTyphoonIntensity(65);
	console.log(message);

	// console.warn() is a good way to print warnings in our console that could help us devs act on certain outpu within our code

	if (message == 'Tropical Strom detected!'){
		console.warn(message);
	}

	// [truthy and falsy]

	/*
		- In JS a "truthy" value is a value that is considered true when encountered in a Boolean Context
		-Values are considered true unless defined false

		-False values/exceptions for truthy:
			1. false
			2. 0
			3. -0
			4. ""
			5. null
			6. indefined
			7. NaN
	*/

	// Truthy Examples
	if(true){
		console.log('This is truthy!')
	}

	if (1){
		console.log('This is truthy!')
	}
	if ([]){
		console.log('This is truthy!')
	}

	// Falsy Examples
	if (false){
		console.log('This will not log in the console')
	}
	if (0){
		console.log('This will not log in the console')
	}

	if (undefined){
		console.log('This will not log in the console')
	}

	if("kwento"){
		console.log("Kwento mo yan eh!")
	}

// [condition (ternary) operator]
/*

	Takes in three operands:
	1. condition
	2. expression to execute if the condition is truthy
	3. expression to execute if the condition is falsy

	Syntax:
		(conditon) ? ifTrue : ifFalse
*/

//single statemente execution
	let ternaryResult = (1 < 18) ? true : false;
	console.log("Result of ternary operator: " + ternaryResult);

// multiple statements execution

	let name;

	function isOfLegalAge(){
		name = 'John';
		return 'You are of the legal age limit!'
	}

	function isUnderAge(){
		name = "Jane";
		return "You are under the age limit"
	}

	let age = parseInt(prompt("What is your age?"));
	console.log(age);

	let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
	console.log("Result of Ternary Operator in functions: " + legalAge + ", " + name);

// [switch statement]
	// can be used as an alternative to an if, else if, and else statement where the data to be used in the condition is of an EXPECTED input

	/*
		Syntax
			switch (expression){
	
				case value:
					statement;
					break;
				default:
					statement;
					break;
			}
	*/

	let day = prompt("What day of the week is it today?").toLowerCase();
	console.log(day);

		// Create cases for thursday, friday, saturday, sunday


		switch (day) {

			case 'monday':
				console.log("The color of the day is blue!");
				break;
			case 'tuesday':
				console.log("The color of the day is yellow!");
				break;
			case 'wednesday':
				console.log("The color of the day is red!");
				break;
				
			default:
				console.log("Please input a valid day")
				break;
		}