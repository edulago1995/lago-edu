console.log("JavaScript Loops!");

/*
	MA (3mins.)
	1. Create a function named greeting() and display the message you want to say to yourself using console.log inside of the function
	2. Invoke the greeting() function 5 times
	3. Take a screenshot 

*/

function greeting(){

	console.log("Keep on progressing!")

};

let countNum = 5;

while(countNum !== 0){
	console.log("This is printed inside the sample loop:" + countNum);
	greeting()
	countNum--;
}

//[While Loop]
//takes in an expression/condition
//expressions are any unit of code that can be evaluated to a value
//If the condition evaluates to true, the statements inside the code block will be executed
//A loop will iterate to a certain number of times until an expression or condition is met
//Iteration - is the term given to the repetition of statements

/*
	Syntax:
	while(expression/condition){
		statement
	}
*/

let count = 5;
	
//While the value of count is not equal to 0
while(count !== 0){
	//The current value of count is printed out
	console.log("While: " + count);
	//Decreases the value of count by 1 every iteration to stop the loop when it reaches 0
	count--;
}

//[Do While Loop]
/*
	- this works a lot like the while loop
	- but unlike the while loops, do-while loops guarantee that the code will be executed at least once
	-Syntax
	do {
		statement
	}while(expression/condition)
*/

let number = Number(prompt("Give me a number: "));

do {
	console.log("Do While:" + number);
	//increase the value of number by 1 after every iteration to stop the loop when it reaches 10 or greater
	//number = number + 1
	number += 1;


}while(number < 10)


//[For Loop]
/*
	A for loop is more flexible than while and do-while loops
	It consists of three parts:
	1. initialization - value that will track the progression of the loop
	2. expression/condition - that will be evaluated which will determine whether the loop will run one more time
	3. finalExpression - indicates how to advance the loop

	Syntax

		for (initialization; expression/condition; finalExpression){
			statement
		}

*/

/*
	- Will create a loop that will start from 0 and end at 20
	- Every iteration of the loop, the value of count will be checked if it is equal or less than 20
	- If the value of count is less than or equal to 20, the statement inside of the loop wil execute
	- The value of count will be incremented by one for each iteration
*/

	for (let count = 0; count <= 20; count++){
		console.log("For: " + count);
	}

//[Strings]

	let myString = "Taylor Swift"
	//characters in strings may be counted using  the .length property
	console.log(myString.length);//12 characters

	//accessing elements of a string
	console.log(myString[0]);//T
	console.log(myString[1]);//a
	console.log(myString[2]);//y

	//will create a loop that will print out the individual letters of the myString variable
	for(let x = 0; x < myString.length; x++){
		console.log(myString[x])
	}

//Create a string named "myName" with a value of your name

	let myName = "Cardo";

	/*
		Create a loop that will print out the letters of the name individually and print out the number 3 instead when the letter to be printed out is a vowel
	*/

	for(let i=0; i<myName.length; i++){

		//console.log(myName[i].toLowerCase());

		if(
			myName[i].toLowerCase() == "a" ||
			myName[i].toLowerCase() == "e" ||
			myName[i].toLowerCase() == "i" ||
			myName[i].toLowerCase() == "o" ||
			myName[i].toLowerCase() == "u"
		){
			console.log(3);
		}

		else {
			console.log(myName[i]);
		}
	}

//[Continue and Break Statements]
/*
	- continue statement allows the code to go to the next iteration of the loop without finishing the execution of all statements in a code block
	- break statement is used to terminate the current loop once a match has been found

*/
	/*
		create a loop that if the count value is divisible by 2 and the remainder is 0, it will print the number and continue to the next iteration of the loop
	*/

for (let count = 0; count <= 20; count++){

	if(count % 2 === 0){
		continue;
	}
	console.log("Continue and Break: " + count);

	if(count>10){
		break;
	}
}

let name = "Bernardo";


for (let i = 0; i < name.length; i++) {

    
    // If the vowel is equal to a, continue to the next iteration of the loop
    if (name[i].toLowerCase() === "a") {

        console.log("Continue to the next iteration");
        continue;
    }

    // The current letter is printed out based on it's index
    console.log(name[i]);

    // If the current letter is equal to d, stop the loop
    if (name[i] == "d") {
        break;
    }