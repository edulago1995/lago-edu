//CRUD Operations
/*
	- CRUD operations are the heart of any backend application
	- Mastering the CRUD operations is essential for any developer
	-Mastering the CRUD operations of any language makes us a valuable developer and makes the work easier for us to deal with huge amounts of information
*/

//to switch dbs, we use - use <dbName>

//[Create] Insert documents
	//Insert one document
		//Syntax:
			//db.collectionName.insertOne({object});
	
	db.users.insertOne({
		"firstName":"John",
		"lastName":"Smith"
	})

	db.users.insertOne({
		firstName: "Jane",
		lastName: "Doe",
		age: 21,
		contact:{
			phone: "87000",
			email:"janedoe@gmail.com"
		},
		courses:["CSS","JavaScript","Python"],
		department: "none"
	})

	//Insert Many
	/*
		Syntax
			db.collectionName.insertMany([{objectA},{objectB}])
	*/

	db.users.insertMany([
			{
				firstName: "Stephen",
				lastName: "Hawking",
				age: 76,
				contact:{
					phone: "87001",
					email:"stephenhawking@gmail.com"
				},
				courses:["Python","React","PHP"],
				department: "none"			
			},
			{
				firstName:"Neil",
				lastName:"Armstrong",
				age: 82,
				contact: {
					phone: "87002",
					email:"neilarmstrong@gmail.com"
				},
				courses:["React","Laravel","Sass"],
				department: "none"
			}
		])


//[Read] Finding Documents

//Find
	/*
		Syntax:
		db.collectionName.find()
		db.collectionName.find({field:value})
	*/

//Finding a single document
	//leaving the search criteria empty will retrieve ALL the documents
	db.users.find()

	db.users.find({firstName:"Stephen"})

//Finding document with multiple parameters
/*
	Syntax:
		db.collectionName.find({fieldA: valueA, fieldB:valueB})
*/

	db.users.find({lastName:"Armstrong",age:82})

//[Update] Updating a document


//Updating a single Document

	//let's add another document
	db.users.insertOne({
		firstName: "Test",
		lastName:"Test",
		age: 0,
		contact:{
			phone: "00000",
			email:"test@gmail.com"
		},
		courses: [],
		department: "none"
	})

	/*
		
		Syntax:
			db.collectionName.updateOne({criteria},{$set:{field,value}});

	*/

	//$set operator replaces the value of a field with the specified value

	db.users.updateOne(
		{firstName: "Test"},
		{
			$set:{
				firstName: "Bill",
				lastName: "Gates",
				age: 65,
				contact:{
					phone: "12345678",
					email:"bill@gmail.com"
				},
				courses:["PHP","Laravel","HTML"],
				department:"Operations",
				status: "active"
			}
		}
	)
	
	db.users.find({firstName:"Bill"})
	//status is inserted automatically

//Updating multiple documents
/*
	Syntax:
		db.collectionName.updateMany({criteria},{$set:{field,value}})

		

*/

	db.users.updateMany(
		{department: "none"},

		{
			$set:{department:"HR"}
		}
	)



//[Delete] Deleting documents

//create a document to delete

	db.users.insert({
		firstName:"test"
	})

//Deleting a single document
	/*
		Syntax
		db.collectionName.deleteOne({criteria})
	*/

	db.users.deleteOne({
		firstName:"test"
	})

//Delete Many
	/*
		Be careful when using the deleteMany method. If we do not add a search criteria, it will DELETE ALL DOCUMENTS in a db
		DO NOT USE: databaseName.collectionName.deleteMany()
		Syntax:
		db.collectionName.deleteMany({criteria})

	*/

	db.users.deleteMany({
		firstName: "Bill"
	})


//[Take Home Review]

//[replaceOne]
	db.users.insertOne(

		{
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact:{
				phone: "12345678",
				email:"bill@gmail.com"
			},
			courses:["PHP","Laravel","HTML"],
			department:"Operations",
			status: "active"
		}

	)

// Replace One
/*
    - Can be used if replacing the whole document is necessary.
    - Syntax
        - db.collectionName.replaceOne( {criteria}, {$set: {field: value}});
*/

db.users.replaceOne(
    { firstName: "Bill" },
    {
        firstName: "Bill",
        lastName: "Gates",
        age: 65,
        contact: {
            phone: "12345678",
            email: "bill@gmail.com"
        },
        courses: ["PHP", "Laravel", "HTML"],
        department: "Operations"
    }
)


//[Advanced Queries]

/*
    - Retrieving data with complex data structures is also a good skill for any developer to have.
    - Real world examples of data can be as complex as having two or more layers of nested objects and arrays.
    - Learning to query these kinds of data is also essential to ensure that we are able to retrieve any information that we would need in our application
*/

// Query an embedded document
db.users.find({
    contact: {
        phone: "87654321",
        email: "stephenhawking@gmail.com"
    }
})

// Query on nested field
db.users.find(
    {"contact.email": "janedoe@gmail.com"}
)

// Querying an Array with Exact Elements
db.users.find( { courses: [ "CSS", "Javascript", "Python" ] } )

// Querying an Array without regard to order
db.users.find( { courses: { $all: ["React", "Python"] } } )
//$all operator selects the documents where the value of a field is an array that contains all the specified elements. 


// Querying an Embedded Array
db.users.insert({
    namearr: [
        {
            namea: "juan"
        },
        {
            nameb: "tamad"
        }
    ]
})

db.users.find({
    namearr: 
        {
            namea: "juan"
        }
})