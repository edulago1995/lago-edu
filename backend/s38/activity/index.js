const http = require("http");

const port = 4000;

const app = http.createServer((request, response) => {
  if (request.url === "/" && request.method === "GET") {
    response.writeHead(200, { "Content-type": "text/plain" });
    response.write("Welcome to the booking system");
    response.end();
  } else if (request.url === "/profile" && request.method === "GET") {
    response.writeHead(200, { "Content-type": "text/plain" });
    response.write("Welcome to your profile");
    response.end();
  } else if (request.url === "/courses" && request.method === "GET") {
    response.writeHead(200, { "Content-type": "text/plain" });
    response.write("Here's our available courses");
    response.end();
  } else if (request.url === "/addCourses" && request.method === "POST") {
    response.writeHead(200, { "Content-type": "text/plain" });
    response.write("Add course to our resources");
    response.end();
  } else if (request.url === "/updateCourses" && request.method === "PUT") {
    response.writeHead(200, { "Content-type": "text/plain" });
    response.write("Update a course to our resources");
    response.end();
  } else if (request.url === "/archiveCourses" && request.method === "DELETE") {
    response.writeHead(200, { "Content-type": "text/plain" });
    response.write("Archive course to our resources");
    response.end();
  } 
});


//Do not modify
//Make sure to save the server in variable called app
if(require.main === module){
    app.listen(4000, () => console.log(`Server running at port 4000`));
}

module.exports = app;