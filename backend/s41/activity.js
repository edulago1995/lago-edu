const express = require("express");
const mongoose = require('mongoose');

const app = express();
const port = 3001;

app.use(express.json());
app.use(express.urlencoded({extended: true}));

mongoose.connect("mongodb+srv://b297edu:HSgtfWvjiRNXyRlA@batch-297.bc8fzg3.mongodb.net/userDB?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database"))

const userSchema = new mongoose.Schema({

	username: String,
	password: String,
});


const User = mongoose.model("User", userSchema)

app.post("/signup",(req,res)=>{
	User.findOne({username: req.body.username}).then((result, err) => {

		if(result != null && result.username == req.body.username){
			return res.send("Duplicate user found");
		} else {
			if (req.body.username !== "" && req.body.password !== "") {
				let newUser = new User({
					username: req.body.username
				});

				newUser.save().then((savedUser, saveErr) => {
					if(saveErr){
						return console.error(saveErr);
					} else {
						return res.status(201).send("New user created");
					}
				})
			}else{
				res.send("BOTH username and password must be provided")
			}
		}
	})
})

app.get("/signup", (req, res) => {
	User.find({}).then((result, err) => {
		if(err){
			return console.log(err);
		} else {
			return res.status(200).json({
				Users: result
			});
		}
	})
});

if(require.main === module){
	app.listen(port, () => console.log(`Server running at port ${port}`));
}

module.exports = app;