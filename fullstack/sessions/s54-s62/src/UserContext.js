import React from 'react';

// Creates a Context object

const UserContext = React.createContext();
//allows us to share data between components (without using props)

// Provider component
export const UserProvider = UserContext.Provider;
//provided data to components through the use of context

export default UserContext;