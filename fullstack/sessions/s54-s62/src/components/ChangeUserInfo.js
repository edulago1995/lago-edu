import React, { useState, useEffect } from 'react';
import Swal from 'sweetalert2';

const ChangeUserInfo = () => {
  const [userInfo, setUserInfo] = useState({
    firstName: '',
    lastName: '',
    mobileNo: '',
  });
  const [updatedUserInfo, setUpdatedUserInfo] = useState(null); // Store the updated user info

  const handleChange = (e) => {
    const { name, value } = e.target;
    setUserInfo((prevUserInfo) => ({
      ...prevUserInfo,
      [name]: value,
    }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const token = localStorage.getItem('token');
      const response = await fetch('http://localhost:4000/users/profile', {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify(userInfo),
      });

      if (response.ok) {
        const updatedData = await response.json();
        setUpdatedUserInfo(updatedData); // Update local state with the new data
        Swal.fire({
          title: 'Success',
          icon: 'success',
          text: 'User info updated successfully!',
        });
      } else {
        Swal.fire({
          title: 'Error',
          icon: 'error',
          text: 'Failed to update user info.',
        });
      }
    } catch (error) {
      console.error('Error updating user info:', error);
    }
  };

  useEffect(() => {
    // This effect will run when updatedUserInfo changes to update the form fields.
    if (updatedUserInfo) {
      setUserInfo(updatedUserInfo); // Update the form fields with the new data
    }
  }, [updatedUserInfo]);

  return (
    <div className="container mt-4">
      <h2>Update User Info</h2>
      <form onSubmit={handleSubmit}>
        <div className="mb-3">
          <label htmlFor="firstName" className="form-label">
            First Name
          </label>
          <input
            type="text"
            className="form-control"
            id="firstName"
            name="firstName"
            value={userInfo.firstName}
            onChange={handleChange}
          />
        </div>
        <div className="mb-3">
          <label htmlFor="lastName" className="form-label">
            Last Name
          </label>
          <input
            type="text"
            className="form-control"
            id="lastName"
            name="lastName"
            value={userInfo.lastName}
            onChange={handleChange}
          />
        </div>
        <div className="mb-3">
          <label htmlFor="mobileNo" className="form-label">
            Mobile Number
          </label>
          <input
            type="text"
            className="form-control"
            id="mobileNo"
            name="mobileNo"
            value={userInfo.mobileNo}
            onChange={handleChange}
          />
        </div>
        <button type="submit" className="btn btn-primary">
          Update Info
        </button>
      </form>
    </div>
  );
};

export default ChangeUserInfo;